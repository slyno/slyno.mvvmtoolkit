﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Slyno.MVVMToolkit.SlynoConverters
{
    /// <summary>
    /// Value converter that translates null to <see cref="Visibility.Collapsed"/> and not null to
    /// <see cref="Visibility.Visible"/>.
    /// </summary>
    public sealed class NullOrEmptyToCollapsedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (string.IsNullOrEmpty(value?.ToString())) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
