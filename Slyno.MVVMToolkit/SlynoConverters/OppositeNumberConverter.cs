﻿using System;
using Windows.UI.Xaml.Data;

namespace Slyno.MVVMToolkit.SlynoConverters
{
    public sealed class OppositeNumberConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return double.Parse(value.ToString()) * -1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return double.Parse(value.ToString()) * -1;
        }
    }
}
