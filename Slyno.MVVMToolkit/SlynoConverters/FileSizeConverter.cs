﻿using System;
using Windows.UI.Xaml.Data;

namespace Slyno.MVVMToolkit.SlynoConverters
{
    /// <summary>
    /// Converts a long into a file size showing B, KB, MB, or GB.
    /// </summary>
    public sealed class FileSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
            {
                return GetSizeString(0);
            }

            ulong size;

            ulong.TryParse(value.ToString(), out size);

            return GetSizeString(size);
        }

        private static string GetSizeString(ulong size)
        {
            const double b = 1024;
            const double kb = b * 1024;
            const double mb = kb * 1024;

            if (size < b)
            {
                return string.Format("{0} B", (size / b).ToString("#,###"));
            }

            if (size < kb)
            {
                return string.Format("{0} KB", (size / b).ToString("#,###,###"));
            }

            if (size < mb)
            {
                return string.Format("{0} MB", (size / kb).ToString("#,###,###.00"));
            }

            return string.Format("{0} GB", (size / kb).ToString("#,###,###.00"));
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
