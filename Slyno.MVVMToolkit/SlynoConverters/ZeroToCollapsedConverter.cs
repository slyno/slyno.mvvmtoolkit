﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Slyno.MVVMToolkit.SlynoConverters
{
    /// <summary>
    /// Converts the value to Visible if the value is greater then zero, or Collapsed if it's null or zero.
    /// </summary>
    public sealed class ZeroToCollapsedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var count = value as int?;
            if (!count.HasValue)
            {
                return Visibility.Collapsed;
            }

            return count > 0 ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
