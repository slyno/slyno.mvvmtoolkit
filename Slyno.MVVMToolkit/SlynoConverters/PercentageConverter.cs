﻿using System;
using Windows.UI.Xaml.Data;

namespace Slyno.MVVMToolkit.SlynoConverters
{
    /// <summary>
    /// Converts a double such as .1 and converts it to 10 and back.
    /// </summary>
    public sealed class PercentageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            double percent = 0;

            if (value == null)
            {
                return percent;
            }

            double.TryParse(value.ToString(), out percent);

            return percent * 100.0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            double percent = 0;

            if (value == null)
            {
                return percent;
            }

            double.TryParse(value.ToString(), out percent);

            return percent / 100.0;
        }
    }
}
