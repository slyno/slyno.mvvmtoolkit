﻿using System;
using Windows.UI.Xaml.Data;

namespace Slyno.MVVMToolkit.SlynoConverters
{
    /// <summary>
    /// Converts the given number to a scale of the number by the given parameter and back.
    /// Ex. value = 2, parameter = .5 > Convert will be 1.
    /// </summary>
    public sealed class ScaleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            double scale;
            if (!double.TryParse(parameter.ToString(), out scale))
            {
                scale = 1;
            }

            double val;
            if (!double.TryParse(value.ToString(), out val))
            {
                return null;
            }

            return val * scale;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            double scale;
            if (!double.TryParse(parameter.ToString(), out scale))
            {
                scale = 1;
            }

            double val;
            if (!double.TryParse(value.ToString(), out val))
            {
                return null;
            }

            return val / scale;
        }
    }
}
