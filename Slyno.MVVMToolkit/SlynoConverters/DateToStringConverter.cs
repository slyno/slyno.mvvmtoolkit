﻿using System;
using Windows.UI.Xaml.Data;

namespace Slyno.MVVMToolkit.SlynoConverters
{
    /// <summary>
    /// Value converter that converts the date using the given format parameter.
    /// </summary>
    public sealed class DateToStringConverter : IValueConverter
    {
        public static string DefaultFormat = "MMMM d, yyyy";

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var format = parameter == null ? DefaultFormat : parameter.ToString();

            return ((DateTime)value).ToString(format);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
            {
                return null;
            }

            DateTime date;
            if (DateTime.TryParse(value.ToString(), out date))
            {
                return date;
            }

            return null;
        }
    }
}
