﻿using System;
using Windows.UI.Xaml.Data;

namespace Slyno.MVVMToolkit.SlynoConverters
{
    /// <summary>
    /// Converts an number to a pluralized string or a singular string.
    /// You must pass a ConverterParameter as 'singularTerm,pluralTerm' Ex: 'Berry,Berries'
    /// </summary>
    public sealed class CountToPluralStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var split = parameter?.ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (split?.Length != 2)
            {
                return null;
            }

            var singularTerm = split[0].Trim();
            var pluralTerm = split[1].Trim();

            if (value == null)
            {
                return pluralTerm;
            }

            int count;
            int.TryParse(value.ToString(), out count);

            return count == 1 ? singularTerm : pluralTerm;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
