﻿using System;
using Windows.UI.Xaml.Data;

namespace Slyno.MVVMToolkit.SlynoConverters
{
    /// <summary>
    /// Converts to true is the value is null, otherwise, false.
    /// </summary>
    public sealed class NullToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return value == null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
