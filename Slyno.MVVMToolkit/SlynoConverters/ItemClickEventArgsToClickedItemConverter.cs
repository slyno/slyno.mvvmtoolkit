﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;

namespace Slyno.MVVMToolkit.SlynoConverters
{
    public sealed class ItemClickEventArgsToClickedItemConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var itemClickEventArgs = value as ItemClickEventArgs;

            return itemClickEventArgs?.ClickedItem;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
