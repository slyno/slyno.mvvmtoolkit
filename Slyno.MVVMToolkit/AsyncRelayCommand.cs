﻿using System;
using System.Threading.Tasks;

namespace Slyno.MVVMToolkit
{
    public class AsyncRelayCommand : RelayCommand
    {
        protected Func<object, Task> AsyncExecuteAction;

        public AsyncRelayCommand(Func<object, Task> asyncExecuteAction) : this(asyncExecuteAction, null) { }

        public AsyncRelayCommand(Func<object, Task> asyncExecuteAction, Predicate<object> canExecuteAction)
            : base(null, canExecuteAction)
        {
            this.AsyncExecuteAction = asyncExecuteAction;
        }

        public override async void Execute(object parameter)
        {
            await this.ExecuteAsync(parameter);
        }

        public virtual async Task ExecuteAsync(object paramenter)
        {
            await this.AsyncExecuteAction(paramenter);
        }
    }
}
