﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace Slyno.MVVMToolkit.EventArgs
{
    public sealed class ElementManipulatedEventArgs : System.EventArgs
    {
        public ElementManipulatedEventArgs(UIElement element, CompositeTransform startTransform, CompositeTransform finalTransform)
        {
            this.Element = element;
            this.StartTransform = startTransform;
            this.FinalTransform = finalTransform;
        }

        public UIElement Element { get; }

        public CompositeTransform StartTransform { get; }

        public CompositeTransform FinalTransform { get; }
    }
}
