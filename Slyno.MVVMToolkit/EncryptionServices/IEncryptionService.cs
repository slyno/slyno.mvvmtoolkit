﻿using System.Threading.Tasks;

namespace Slyno.MVVMToolkit.EncryptionServices
{
    public interface IEncryptionService
    {
        Task<string> EncryptAsync(string plainString);

        Task<string> DecryptAsync(string encryptedString);
    }
}
