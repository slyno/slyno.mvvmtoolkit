﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography.DataProtection;

namespace Slyno.MVVMToolkit.EncryptionServices
{
    /// <summary>
    /// Encrypts and decrypts strings using the DataProtection API's.
    /// All encryption is done using a descriptor of LOCAL=macine so the consuming app and encrypt/decrypt no matter what user the app is running under.
    /// </summary>
    public sealed class DataProtectionEncryptionService : IEncryptionService
    {
        private const string Descriptor = "LOCAL=machine";

        public async Task<string> EncryptAsync(string plainString)
        {
            var provider = new DataProtectionProvider(Descriptor);

            var plainBytes = Encoding.Unicode.GetBytes(plainString);

            using (var source = new MemoryStream(plainBytes))
            {
                using (var destination = new MemoryStream())
                {
                    await provider.ProtectStreamAsync(source.AsInputStream(), destination.AsOutputStream());

                    destination.Position = 0;

                    using (var sr = new StreamReader(destination, Encoding.Unicode))
                    {
                        return await sr.ReadToEndAsync();
                    }
                }
            }
        }

        public async Task<string> DecryptAsync(string encryptedString)
        {
            var provider = new DataProtectionProvider();

            var encryptedBytes = Encoding.Unicode.GetBytes(encryptedString);

            using (var source = new MemoryStream(encryptedBytes))
            {
                using (var destination = new MemoryStream())
                {
                    await provider.UnprotectStreamAsync(source.AsInputStream(), destination.AsOutputStream());

                    destination.Position = 0;

                    using (var sr = new StreamReader(destination, Encoding.Unicode))
                    {
                        return await sr.ReadToEndAsync();
                    }
                }
            }
        }
    }
}
