﻿using System;
using System.Windows.Input;

namespace Slyno.MVVMToolkit
{
    public class RelayCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        protected Predicate<object> CanExecuteAction;
        protected Action<object> ExecuteAction;

        public RelayCommand(Action<object> executeAction) : this(executeAction, null) { }

        public RelayCommand(Action<object> executeAction, Predicate<object> canExecuteAction)
        {
            this.ExecuteAction = executeAction;
            this.CanExecuteAction = canExecuteAction;

            if (canExecuteAction != null)
            {
                this._canExecute = true;
            }
        }

        private readonly bool _canExecute = true;
        public bool CanExecute(object parameter)
        {
            if (this.CanExecuteAction == null)
            {
                return _canExecute;
            }

            var result = this.CanExecuteAction(parameter);

            if (result != this._canExecute)
            {
                this.CanExecuteChanged?.Invoke(this, System.EventArgs.Empty);
            }

            return _canExecute;
        }

        public virtual void Execute(object parameter)
        {
            this.ExecuteAction(parameter);
        }
    }
}
