﻿using System.Threading.Tasks;
using Windows.UI.Popups;

namespace Slyno.MVVMToolkit.DialogServices
{
    public interface IDialogService
    {
        void Show(string message, string title, params IUICommand[] commands);

        void Show(string message);

        Task ShowAsync(string message);

        Task ShowAsync(string message, string title, params IUICommand[] commands);
    }
}
