﻿using System;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace Slyno.MVVMToolkit.DialogServices
{
    public class MessageDialogService : IDialogService
    {
        private bool _open;

        public virtual async void Show(string message, string title, params IUICommand[] commands)
        {
            while (_open)
            {
                await Task.Delay(1000);
            }

            _open = true;

            var dialog = new MessageDialog(message, title);

            foreach (var item in commands)
            {
                dialog.Commands.Add(item);
            }

            await dialog.ShowAsync();

            _open = false;
        }

        public virtual async void Show(string message)
        {
            while (_open)
            {
                await Task.Delay(1000);
            }

            _open = true;

            var dialog = new MessageDialog(message);

            await dialog.ShowAsync();

            _open = false;
        }

        public virtual async Task ShowAsync(string message)
        {
            var dialog = new MessageDialog(message);

            await dialog.ShowAsync();
        }

        public virtual async Task ShowAsync(string message, string title, params IUICommand[] commands)
        {
            var dialog = new MessageDialog(message, title);

            foreach (var item in commands)
            {
                dialog.Commands.Add(item);
            }

            await dialog.ShowAsync();
        }
    }
}
