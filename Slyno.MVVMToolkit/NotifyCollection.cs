﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Slyno.MVVMToolkit
{
    public class NotifyCollection<T> : ObservableCollection<T>
    {
        public NotifyCollection()
        {
        }

        public NotifyCollection(IEnumerable<T> collection) : base(collection)
        {
            this.IsEmpty = !this.Any();
        }

        private bool _isEmpty = true;

        public bool IsEmpty
        {
            get { return _isEmpty; }
            private set
            {
                if (_isEmpty == value)
                {
                    return;
                }

                _isEmpty = value;

                this.NotifyPropertyChanged();
            }
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            base.OnCollectionChanged(e);

            this.IsEmpty = !this.Any();
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (propertyName == null)
            {
                return;
            }

            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
    }
}
