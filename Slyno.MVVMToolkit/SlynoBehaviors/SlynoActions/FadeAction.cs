﻿using System;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Animation;
using Microsoft.Xaml.Interactivity;

namespace Slyno.MVVMToolkit.SlynoBehaviors.SlynoActions
{
    /// <summary>
    /// This action will fade the attached object in or out based on the To setting from the opacity the object is currently at.
    /// You can execute commands when the fade starts and completes.
    /// The default To is 1.0.
    /// The default Duration of the fade is 300 ms.
    /// The default Delay is 0 ms.
    /// The default Easing is none.
    /// </summary>
    public sealed class FadeAction : DependencyObject, IAction
    {
        public static readonly DependencyProperty DurationProperty = DependencyProperty.Register(
            "Duration", typeof(TimeSpan), typeof(FadeAction), new PropertyMetadata(TimeSpan.FromMilliseconds(300)));

        public TimeSpan Duration
        {
            get { return (TimeSpan)GetValue(DurationProperty); }
            set { SetValue(DurationProperty, value); }
        }

        public static readonly DependencyProperty DelayProperty = DependencyProperty.Register(
            "Delay", typeof(TimeSpan), typeof(FadeAction), new PropertyMetadata(TimeSpan.FromMilliseconds(0)));

        public TimeSpan Delay
        {
            get { return (TimeSpan)GetValue(DelayProperty); }
            set { SetValue(DelayProperty, value); }
        }

        public static readonly DependencyProperty ToProperty = DependencyProperty.Register(
            "To", typeof(double), typeof(FadeAction), new PropertyMetadata(1.0));

        public double To
        {
            get { return (double)GetValue(ToProperty); }
            set { SetValue(ToProperty, value); }
        }

        public static readonly DependencyProperty EasingProperty = DependencyProperty.Register(
            "Easing", typeof(EasingFunctionBase), typeof(FadeAction), new PropertyMetadata(default(EasingFunctionBase)));

        public EasingFunctionBase Easing
        {
            get { return (EasingFunctionBase)GetValue(EasingProperty); }
            set { SetValue(EasingProperty, value); }
        }

        public static readonly DependencyProperty StartedCommandProperty = DependencyProperty.Register(
            "StartedCommand", typeof(ICommand), typeof(FadeAction), new PropertyMetadata(default(ICommand)));

        public ICommand StartedCommand
        {
            get { return (ICommand)GetValue(StartedCommandProperty); }
            set { SetValue(StartedCommandProperty, value); }
        }

        public static readonly DependencyProperty CompletedCommandProperty = DependencyProperty.Register(
            "CompletedCommand", typeof(ICommand), typeof(FadeAction), new PropertyMetadata(default(ICommand)));

        public ICommand CompletedCommand
        {
            get { return (ICommand)GetValue(CompletedCommandProperty); }
            set { SetValue(CompletedCommandProperty, value); }
        }

        public object Execute(object sender, object parameter)
        {
            var element = sender as DependencyObject;
            if (element == null)
            {
                return null;
            }

            this.Fade(element);

            return null;
        }

        private void Fade(DependencyObject element)
        {
            var duration = new Duration(this.Duration);

            var animation = new DoubleAnimation
            {
                To = this.To,
                Duration = duration,
            };

            if (this.Easing != null)
            {
                animation.EasingFunction = this.Easing;
            }

            Storyboard.SetTarget(animation, element);
            Storyboard.SetTargetProperty(animation, "Opacity");

            var sb = new Storyboard
            {
                Duration = duration
            };

            if (this.CompletedCommand != null)
            {
                sb.Completed += (s, o) =>
                {
                    this.CompletedCommand?.Execute(null);
                };
            }

            sb.Children.Add(animation);

            sb.Begin();

            this.StartedCommand?.Execute(null);
        }
    }
}
