﻿using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Microsoft.Xaml.Interactivity;

namespace Slyno.MVVMToolkit.SlynoBehaviors
{
    /// <summary>
    /// Adds parallax to the given element if there is a scrollview in it's hierachy.
    /// This will do either vertical or horizontal parallax based on scrolling.
    /// </summary>
    public sealed class ParallaxBehavior : Behavior<FrameworkElement>
    {
        private readonly TranslateTransform _translateTransform = new TranslateTransform();

        private ScrollViewer _scrollViewer;

        public static readonly DependencyProperty ParallaxRatioProperty = DependencyProperty.Register(
            "ParallaxRatio", typeof(double), typeof(ParallaxBehavior), new PropertyMetadata(.15));

        /// <summary>
        /// Value between 0 and .9 that controlls the speed of the parallax.
        /// Default is .15. 0 is no movement and .9 is maximum movement.
        /// </summary>
        public double ParallaxRatio
        {
            get { return (double)GetValue(ParallaxRatioProperty); }
            set
            {
                if (value > .9)
                {
                    value = .9;
                }
                if (value < 0)
                {
                    value = 0;
                }

                SetValue(ParallaxRatioProperty, value);
            }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            var transform = this.AssociatedObject.RenderTransform;
            if (transform == null)
            {
                this.AssociatedObject.RenderTransform = _translateTransform;
            }
            else
            {
                var transformGroup = transform as TransformGroup;

                if (transformGroup == null)
                {
                    transformGroup = new TransformGroup();
                    this.AssociatedObject.RenderTransform = transformGroup;

                    transformGroup.Children.Add(transform);
                }

                transformGroup.Children.Add(_translateTransform);
            }

            this.AssociatedObject.Loaded += AssociatedObjectOnLoaded;
        }

        private void SetupScrollviewer()
        {
            _scrollViewer = ControlFinder.FindParent<ScrollViewer>(this.AssociatedObject);

            if (_scrollViewer == null)
            {
                return;
            }

            _translateTransform.X = _scrollViewer.HorizontalOffset * this.ParallaxRatio;
            _translateTransform.Y = _scrollViewer.VerticalOffset * this.ParallaxRatio;

            _scrollViewer.ViewChanged += ScrollViewerOnViewChanged;
        }

        private void AssociatedObjectOnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            this.AssociatedObject.Loaded -= this.AssociatedObjectOnLoaded;

            this.SetupScrollviewer();

            if (_scrollViewer == null)
            {
                return;
            }

            this.SetupClip();

            var image = this.AssociatedObject as Image;
            if (image != null)
            {
                image.ImageOpened += ImageOnImageOpened;
            }
        }

        private void ImageOnImageOpened(object sender, RoutedEventArgs routedEventArgs)
        {
            var image = sender as Image;
            if (image == null)
            {
                return;
            }

            image.ImageOpened -= ImageOnImageOpened;

            this.SetupClip();
        }

        private void SetupClip()
        {
            var parent = this.AssociatedObject.Parent as FrameworkElement;

            if (parent == null)
            {
                return;
            }

            var behaviors = Interaction.GetBehaviors(parent);
            if (!behaviors.OfType<ClipToBoundsBehavior>().Any())
            {
                behaviors.Add(new ClipToBoundsBehavior());
            }
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            if (_scrollViewer == null)
            {
                return;
            }

            _scrollViewer.ViewChanged -= ScrollViewerOnViewChanged;
        }

        private void ScrollViewerOnViewChanged(object sender, ScrollViewerViewChangedEventArgs scrollViewerViewChangedEventArgs)
        {
            var scrollViewer = sender as ScrollViewer;
            if (scrollViewer == null)
            {
                return;
            }

            _translateTransform.X = scrollViewer.HorizontalOffset * this.ParallaxRatio;
            _translateTransform.Y = scrollViewer.VerticalOffset * this.ParallaxRatio;
        }
    }
}
