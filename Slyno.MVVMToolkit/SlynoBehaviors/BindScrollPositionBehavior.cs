﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Microsoft.Xaml.Interactivity;

namespace Slyno.MVVMToolkit.SlynoBehaviors
{
    /// <summary>
    /// Binds the scroll position of the attached ScrollViewer or a child ScrollViewer to the given Offset properties.
    /// The child ScrollViewer is automatically detected if this behavior is attached to an ItemsControl.
    /// Make sure to set the offset properties to TwoWay binding. This is useful for maintaining scroll position during navigation.
    /// </summary>
    public sealed class BindScrollPositionBehavior : Behavior<FrameworkElement>
    {
        public static readonly DependencyProperty HorizontalOffsetProperty = DependencyProperty.Register(
            "HorizontalOffset", typeof(double), typeof(BindScrollPositionBehavior), new PropertyMetadata(default(double)));

        public double HorizontalOffset
        {
            get { return (double)GetValue(HorizontalOffsetProperty); }
            set { SetValue(HorizontalOffsetProperty, value); }
        }

        public static readonly DependencyProperty VerticalOffsetProperty = DependencyProperty.Register(
            "VerticalOffset", typeof(double), typeof(BindScrollPositionBehavior), new PropertyMetadata(default(double)));

        public double VerticalOffset
        {
            get { return (double)GetValue(VerticalOffsetProperty); }
            set { SetValue(VerticalOffsetProperty, value); }
        }

        public static readonly DependencyProperty ZoomFactorProperty = DependencyProperty.Register(
            "ZoomFactor", typeof(float), typeof(BindScrollPositionBehavior), new PropertyMetadata(1f));

        public float ZoomFactor
        {
            get { return (float)GetValue(ZoomFactorProperty); }
            set { SetValue(ZoomFactorProperty, value); }
        }

        private ScrollViewer _scrollViewer;

        protected override void OnAttached()
        {
            base.OnAttached();

            this.AssociatedObject.Loaded += AssociatedObjectOnLoaded;
        }

        private void AssociatedObjectOnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            this.AssociatedObject.Loaded -= this.AssociatedObjectOnLoaded;

            _scrollViewer = this.AssociatedObject as ScrollViewer ??
                           ControlFinder.FindChild<ScrollViewer>(this.AssociatedObject);

            if (_scrollViewer == null)
            {
                return;
            }

            _scrollViewer.ChangeView(this.HorizontalOffset, this.VerticalOffset, this.ZoomFactor, false);

            _scrollViewer.ViewChanged += ScrollViewerOnViewChanged;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            if (_scrollViewer == null)
            {
                return;
            }

            _scrollViewer.ViewChanged -= ScrollViewerOnViewChanged;
        }

        private void ScrollViewerOnViewChanged(object sender, ScrollViewerViewChangedEventArgs scrollViewerViewChangedEventArgs)
        {
            var scrollViewer = sender as ScrollViewer;
            if (scrollViewer == null)
            {
                return;
            }

            this.VerticalOffset = scrollViewer.VerticalOffset;
            this.HorizontalOffset = scrollViewer.HorizontalOffset;
            this.ZoomFactor = scrollViewer.ZoomFactor;
        }
    }
}
