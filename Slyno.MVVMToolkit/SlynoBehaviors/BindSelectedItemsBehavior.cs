﻿using System.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Microsoft.Xaml.Interactivity;

namespace Slyno.MVVMToolkit.SlynoBehaviors
{
    /// <summary>
    /// Binds the SelectedItems of a ListViewBase such as ListView or GridView to the given SelectedItems property.
    /// </summary>
    public sealed class BindSelectedItemsBehavior : Behavior<ListViewBase>
    {
        public static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.Register(
            "SelectedItems", typeof(IList), typeof(BindSelectedItemsBehavior), new PropertyMetadata(default(IList)));

        public IList SelectedItems
        {
            get { return (IList)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        private void ListViewBaseOnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.SelectedItems == null)
            {
                return;
            }

            foreach (var removedItem in e.RemovedItems)
            {
                this.SelectedItems.Remove(removedItem);
            }

            foreach (var addedItem in e.AddedItems)
            {
                this.SelectedItems.Add(addedItem);
            }
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            this.AssociatedObject.SelectionChanged -= ListViewBaseOnSelectionChanged;
        }
    }
}
