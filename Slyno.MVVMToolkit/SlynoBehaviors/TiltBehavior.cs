﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Animation;
using Microsoft.Xaml.Interactivity;

namespace Slyno.MVVMToolkit.SlynoBehaviors
{
    public sealed class TiltBehavior : Behavior<FrameworkElement>
    {
        private Storyboard _tiltIn;
        private Storyboard _tiltOut;
        private Pointer _capturedPointer;

        public TiltBehavior()
        {
            _tiltIn = new Storyboard();
            _tiltIn.Children.Add(new PointerDownThemeAnimation());

            _tiltOut = new Storyboard();
            _tiltOut.Children.Add(new PointerUpThemeAnimation());
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            //Add the storyboards to the resources. Needed by the ThemeAnimation.
            this.AssociatedObject.Resources.Add("TiltIn", _tiltIn);
            this.AssociatedObject.Resources.Add("TiltOut", _tiltOut);

            //Need a name for the theme animations.
            //Create one if needed. Use GUID for a unique name, and add "c" at the start to be sure it starts with a letter
            if (string.IsNullOrEmpty(this.AssociatedObject.Name))
            {
                this.AssociatedObject.Name = "c" + Guid.NewGuid();
            }

            //Set the target names
            ((PointerDownThemeAnimation)_tiltIn.Children[0]).TargetName = this.AssociatedObject.Name;
            ((PointerUpThemeAnimation)_tiltOut.Children[0]).TargetName = this.AssociatedObject.Name;

            this.AssociatedObject.PointerPressed += PlayTiltIn;
            this.AssociatedObject.PointerReleased += PlayTiltOut;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            this.AssociatedObject.PointerPressed -= PlayTiltIn;
            this.AssociatedObject.PointerReleased -= PlayTiltOut;

            _tiltIn = null;
            _tiltOut = null;
            _capturedPointer = null;
        }

        private void PlayTiltIn(object sender, PointerRoutedEventArgs args)
        {
            //Capture pointer, in case user go outside of the control while keeping pressing
            _capturedPointer = args.Pointer;

            this.AssociatedObject.CapturePointer(_capturedPointer);

            _tiltIn.Stop();
            _tiltIn.Begin();
        }

        private void PlayTiltOut(object sender, PointerRoutedEventArgs args)
        {
            //Release the pointer
            this.AssociatedObject.ReleasePointerCapture(_capturedPointer);

            _tiltOut.Stop();
            _tiltOut.Begin();
        }
    }
}
