﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Microsoft.Xaml.Interactivity;
using Slyno.MVVMToolkit.SlynoBehaviors.SlynoActions;

namespace Slyno.MVVMToolkit.SlynoBehaviors
{
    /// <summary>
    /// If the attached object has a background imagebrush, this will fade it in when it's loaded.
    /// </summary>
    public sealed class BackgroundImageBrushLoadFadeInBehavior : Behavior<Control>
    {
        public static readonly DependencyProperty DurationProperty = DependencyProperty.Register(
            "Duration", typeof(TimeSpan), typeof(BackgroundImageBrushLoadFadeInBehavior), new PropertyMetadata(TimeSpan.FromMilliseconds(300)));

        /// <summary>
        /// Default is 300ms
        /// </summary>
        public TimeSpan Duration
        {
            get { return (TimeSpan)GetValue(DurationProperty); }
            set { SetValue(DurationProperty, value); }
        }

        private ImageBrush _imageBrush;

        protected override void OnAttached()
        {
            base.OnAttached();

            _imageBrush = this.AssociatedObject.Background as ImageBrush;
            if (_imageBrush == null)
            {
                return;
            }

            _imageBrush.Opacity = 0.01;
            _imageBrush.ImageOpened += OnImageDone;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            if (_imageBrush == null)
            {
                return;
            }

            _imageBrush.ImageOpened -= OnImageDone;
        }

        private void OnImageDone(object sender, RoutedEventArgs routedEventArgs)
        {
            var action = new FadeAction
            {
                To = 1,
                Duration = this.Duration
            };

            action.Execute(_imageBrush, null);
        }
    }
}
