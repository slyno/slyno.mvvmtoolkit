﻿using System;
using Windows.Foundation;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Microsoft.Xaml.Interactivity;
using Slyno.MVVMToolkit.EventArgs;

namespace Slyno.MVVMToolkit.SlynoBehaviors
{
    /// <summary>
    /// Attach this behavior to any FrameworkElement to allow it to be manipulated by touch and wheel.
    /// The object can be moved all around it's parent including rotation and scale based on the settings.
    /// Default MaxScale is 10.
    /// Default MinScale is .2.
    /// Default AlloRotation is true.
    /// Default AllowScale is true.
    /// Default IsParentClip is true. All other clips default to false.
    /// If inside a ScrollViewer, set ZoomFactor equal to the ScrollViewer's ZoomFactor so all the scaling works properly. 
    /// </summary>
    public sealed class ManipulatableBehavior : Behavior<FrameworkElement>
    {
        public event EventHandler<ElementManipulatedEventArgs> ElementManipulated = delegate { };

        private CompositeTransform _startTransform;
        private CompositeTransform _finalTransform;
        private FrameworkElement _parent;

        private readonly DispatcherTimer _transformTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };

        public ManipulatableBehavior()
        {
            _transformTimer.Tick += (sender, o) => { TransformTimerOnTick(); };
        }

        public static readonly DependencyProperty MaxScaleProperty = DependencyProperty.Register(
            "MaxScale", typeof(double), typeof(ManipulatableBehavior), new PropertyMetadata(10.0));

        public double MaxScale
        {
            get { return (double)GetValue(MaxScaleProperty); }
            set { SetValue(MaxScaleProperty, value); }
        }

        public static readonly DependencyProperty MinScaleProperty = DependencyProperty.Register(
            "MinScale", typeof(double), typeof(ManipulatableBehavior), new PropertyMetadata(.2));

        public double MinScale
        {
            get { return (double)GetValue(MinScaleProperty); }
            set { SetValue(MinScaleProperty, value); }
        }

        public static readonly DependencyProperty AllowRotationProperty = DependencyProperty.Register(
            "AllowRotation", typeof(bool), typeof(ManipulatableBehavior), new PropertyMetadata(true));

        public bool AllowRotation
        {
            get { return (bool)GetValue(AllowRotationProperty); }
            set { SetValue(AllowRotationProperty, value); }
        }

        public static readonly DependencyProperty AllowScaleProperty = DependencyProperty.Register(
            "AllowScale", typeof(bool), typeof(ManipulatableBehavior), new PropertyMetadata(true));

        public bool AllowScale
        {
            get { return (bool)GetValue(AllowScaleProperty); }
            set { SetValue(AllowScaleProperty, value); }
        }

        public static readonly DependencyProperty IsRightCenterClipProperty = DependencyProperty.Register(
            "IsRightCenterClip", typeof(bool), typeof(ManipulatableBehavior), new PropertyMetadata(default(bool),
                (sender, args) =>
                {
                    var behavoir = (ManipulatableBehavior)sender;

                    var value = (bool)args.NewValue;

                    behavoir.IsRightCenterClip = value;

                    if (value)
                    {
                        behavoir.IsLeftCenterClip = false;
                        behavoir.IsTopCenterClip = false;
                        behavoir.IsBottomCenterClip = false;
                        behavoir.IsParentClip = true;
                    }

                    behavoir.ResetClip();
                }));

        public bool IsRightCenterClip
        {
            get { return (bool)GetValue(IsRightCenterClipProperty); }
            set { SetValue(IsRightCenterClipProperty, value); }
        }

        public static readonly DependencyProperty IsLeftCenterClipProperty = DependencyProperty.Register(
            "IsLeftCenterClip", typeof(bool), typeof(ManipulatableBehavior), new PropertyMetadata(default(bool),
                (sender, args) =>
                {
                    var behavoir = (ManipulatableBehavior)sender;

                    var value = (bool)args.NewValue;

                    behavoir.IsLeftCenterClip = value;

                    if (value)
                    {
                        behavoir.IsRightCenterClip = false;
                        behavoir.IsTopCenterClip = false;
                        behavoir.IsBottomCenterClip = false;
                        behavoir.IsParentClip = true;
                    }

                    behavoir.ResetClip();
                }));

        public bool IsLeftCenterClip
        {
            get { return (bool)GetValue(IsLeftCenterClipProperty); }
            set { SetValue(IsLeftCenterClipProperty, value); }
        }

        public static readonly DependencyProperty IsTopCenterClipProperty = DependencyProperty.Register(
            "IsTopCenterClip", typeof(bool), typeof(ManipulatableBehavior), new PropertyMetadata(default(bool),
                (sender, args) =>
                {
                    var behavoir = (ManipulatableBehavior)sender;

                    var value = (bool)args.NewValue;

                    behavoir.IsTopCenterClip = value;

                    if (value)
                    {
                        behavoir.IsRightCenterClip = false;
                        behavoir.IsLeftCenterClip = false;
                        behavoir.IsBottomCenterClip = false;
                        behavoir.IsParentClip = true;
                    }

                    behavoir.ResetClip();
                }));

        public bool IsTopCenterClip
        {
            get { return (bool)GetValue(IsTopCenterClipProperty); }
            set { SetValue(IsTopCenterClipProperty, value); }
        }

        public static readonly DependencyProperty IsBottomCenterClipProperty = DependencyProperty.Register(
            "IsBottomCenterClip", typeof(bool), typeof(ManipulatableBehavior), new PropertyMetadata(default(bool),
                (sender, args) =>
                {
                    var behavoir = (ManipulatableBehavior)sender;

                    var value = (bool)args.NewValue;

                    behavoir.IsBottomCenterClip = value;

                    if (value)
                    {
                        behavoir.IsRightCenterClip = false;
                        behavoir.IsLeftCenterClip = false;
                        behavoir.IsTopCenterClip = false;
                        behavoir.IsParentClip = true;
                    }

                    behavoir.ResetClip();
                }));

        public bool IsBottomCenterClip
        {
            get { return (bool)GetValue(IsBottomCenterClipProperty); }
            set { SetValue(IsBottomCenterClipProperty, value); }
        }

        public static readonly DependencyProperty IsParentClipProperty = DependencyProperty.Register(
            "IsParentClip", typeof(bool), typeof(ManipulatableBehavior), new PropertyMetadata(true,
                (sender, args) =>
                {
                    var behavoir = (ManipulatableBehavior)sender;

                    var value = (bool)args.NewValue;

                    behavoir.IsParentClip = value;

                    if (value)
                    {
                        behavoir.IsRightCenterClip = false;
                        behavoir.IsLeftCenterClip = false;
                        behavoir.IsTopCenterClip = false;
                        behavoir.IsBottomCenterClip = false;
                    }

                    behavoir.ResetClip();
                }));

        public bool IsParentClip
        {
            get { return (bool)GetValue(IsParentClipProperty); }
            set { SetValue(IsParentClipProperty, value); }
        }

        public float ZoomFactor { get; set; } = 1.0f;

        protected override void OnAttached()
        {
            base.OnAttached();

            this.AssociatedObject.ManipulationMode = ManipulationModes.Rotate | ManipulationModes.Scale | ManipulationModes.TranslateX | ManipulationModes.TranslateY;
            this.AssociatedObject.ManipulationStarted += ElementOnManipulationStarted;
            this.AssociatedObject.ManipulationDelta += ElementOnManipulationDelta;
            this.AssociatedObject.PointerWheelChanged += ElementOnPointerWheelChanged;
            this.AssociatedObject.ManipulationCompleted += ElementOnManipulationCompleted;

            this.AssociatedObject.Loaded += ElementOnLoaded;

            _startTransform = DeepCopyCompositeTransform(this.GetTransform());

            var image = this.AssociatedObject as Image;
            if (image != null)
            {
                image.ImageOpened += ImageOnImageOpened;
            }
        }

        private void ElementOnManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            if (_transformTimer.IsEnabled)
            {
                TransformTimerOnTick();
            }

            _startTransform = DeepCopyCompositeTransform(this.GetTransform());
        }

        private void ElementOnManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            _finalTransform = DeepCopyCompositeTransform(this.GetTransform());

            this.ElementManipulated(this, new ElementManipulatedEventArgs(this.AssociatedObject, _startTransform, _finalTransform));
        }

        private void ElementOnManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            var delta = e.Delta;

            var transform = this.GetTransform();

            transform.TranslateX += delta.Translation.X / this.ZoomFactor;
            transform.TranslateY += delta.Translation.Y / this.ZoomFactor;

            if (this.AllowRotation)
            {
                transform.Rotation += delta.Rotation;
            }

            if (this.AllowScale)
            {
                transform.ScaleX = transform.ScaleY = Boundry(transform.ScaleX * delta.Scale, MinScale, MaxScale);
            }
        }

        private void ElementOnPointerWheelChanged(object sender, PointerRoutedEventArgs e)
        {
            var props = e.GetCurrentPoint(this.AssociatedObject).Properties;

            if (props.IsHorizontalMouseWheel)
            {
                return;
            }

            var scaleKey = Window.Current.CoreWindow.GetKeyState(VirtualKey.Control);
            var rotateKey = Window.Current.CoreWindow.GetKeyState(VirtualKey.Shift);

            var transform = this.GetTransform();

            if (this.AllowScale && scaleKey.HasFlag(CoreVirtualKeyStates.Down))
            {
                var scale = transform.ScaleX + (props.MouseWheelDelta * .0001);
                transform.ScaleX = transform.ScaleY = Boundry(scale, MinScale, MaxScale);
                this.RunTransformTimer();
            }

            if (this.AllowRotation && rotateKey.HasFlag(CoreVirtualKeyStates.Down))
            {
                transform.Rotation += (props.MouseWheelDelta * .01);
                this.RunTransformTimer();
            }
        }

        private void ElementOnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            this.AssociatedObject.Loaded -= ElementOnLoaded;

            _parent = this.AssociatedObject.Parent as FrameworkElement;
            if (_parent == null)
            {
                return;
            }

            this.ResetClip();
            _parent.SizeChanged += ParentOnSizeChanged;
        }

        private void ParentOnSizeChanged(object sender, SizeChangedEventArgs sizeChangedEventArgs)
        {
            this.ResetClip();
        }

        private static void ImageOnImageOpened(object sender, RoutedEventArgs routedEventArgs)
        {
            var image = sender as Image;
            if (image == null)
            {
                return;
            }

            image.ImageOpened -= ImageOnImageOpened;

            image.RenderTransform = null;
        }


        private void RunTransformTimer()
        {
            if (!_transformTimer.IsEnabled)
            {
                _startTransform = DeepCopyCompositeTransform(this.GetTransform());
            }

            _transformTimer.Stop();
            _transformTimer.Start();
        }

        private void TransformTimerOnTick()
        {
            _transformTimer.Stop();

            _finalTransform = DeepCopyCompositeTransform(this.GetTransform());

            this.ElementManipulated(this, new ElementManipulatedEventArgs(this.AssociatedObject, _startTransform, _finalTransform));
        }

        private void ResetClip()
        {
            if (_parent == null)
            {
                return;
            }

            if (!this.IsParentClip)
            {
                _parent.Clip = null;
            }

            var width = (this.IsRightCenterClip || this.IsLeftCenterClip) ? _parent.ActualWidth / 2 : _parent.ActualWidth;
            var height = (this.IsTopCenterClip || this.IsBottomCenterClip) ? _parent.ActualHeight / 2 : _parent.ActualHeight;

            var top = this.IsTopCenterClip ? _parent.ActualHeight / 2 : 0;
            var left = this.IsLeftCenterClip ? _parent.ActualWidth / 2 : 0;

            _parent.Clip = new RectangleGeometry
            {
                Rect = new Rect(left, top, width, height)
            };
        }


        protected override void OnDetaching()
        {
            base.OnDetaching();

            _transformTimer.Stop();

            this.AssociatedObject.ManipulationStarted -= ElementOnManipulationStarted;
            this.AssociatedObject.ManipulationDelta -= ElementOnManipulationDelta;
            this.AssociatedObject.PointerWheelChanged -= ElementOnPointerWheelChanged;
            this.AssociatedObject.ManipulationCompleted -= ElementOnManipulationCompleted;

            if (_parent != null)
            {
                _parent.SizeChanged -= ParentOnSizeChanged;
            }
        }

        private CompositeTransform GetTransform()
        {
            this.AssociatedObject.RenderTransformOrigin = new Point(.5, .5);

            var tran = (this.AssociatedObject.RenderTransform as CompositeTransform) ?? (CompositeTransform)(this.AssociatedObject.RenderTransform = new CompositeTransform());

            return tran;
        }

        private static double Boundry(double value, double min, double max)
        {
            if (value < min)
            {
                return min;
            }

            return value > max ? max : value;
        }

        private static CompositeTransform DeepCopyCompositeTransform(CompositeTransform ct)
        {
            var copy = new CompositeTransform
            {
                Rotation = ct.Rotation,
                TranslateX = ct.TranslateX,
                TranslateY = ct.TranslateY,
                ScaleX = ct.ScaleX,
                ScaleY = ct.ScaleY
            };

            return copy;
        }
    }
}
