﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Microsoft.Xaml.Interactivity;
using Slyno.MVVMToolkit.SlynoBehaviors.SlynoActions;

namespace Slyno.MVVMToolkit.SlynoBehaviors
{
    /// <summary>
    /// This will fade the given image in as soon as it has been opened.
    /// </summary>
    public sealed class ImageLoadFadeInBehavior : Behavior<Image>
    {
        public static readonly DependencyProperty DurationProperty = DependencyProperty.Register(
            "Duration", typeof(TimeSpan), typeof(ImageLoadFadeInBehavior), new PropertyMetadata(TimeSpan.FromMilliseconds(300)));

        /// <summary>
        /// Default is 300ms
        /// </summary>
        public TimeSpan Duration
        {
            get { return (TimeSpan)GetValue(DurationProperty); }
            set { SetValue(DurationProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            //start to opacity out very dim so the image will still be on screen.
            this.AssociatedObject.Opacity = 0.01;
            this.AssociatedObject.ImageOpened += OnImageOpened;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            this.AssociatedObject.ImageOpened -= OnImageOpened;
        }

        private void OnImageOpened(object sender, RoutedEventArgs routedEventArgs)
        {
            var action = new FadeAction
            {
                To = 1,
                Duration = this.Duration
            };

            action.Execute(this.AssociatedObject, null);
        }
    }
}
