﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Microsoft.Xaml.Interactivity;

namespace Slyno.MVVMToolkit.SlynoBehaviors
{
    /// <summary>
    /// This will zoom the parent ScrollViewer to fit the actual size of the Image.
    /// This behavior must be attached to an Image and the Image must be a chiled of a ScrollViewer.
    /// All sizing is done by creating an event listener on the OnImageOpened event so the actual size will be known.
    /// </summary>
    public sealed class ImageLoadZoomToFitBehavior : Behavior<Image>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            this.AssociatedObject.ImageOpened += OnImageOpened;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            this.AssociatedObject.ImageOpened -= OnImageOpened;
        }

        private void OnImageOpened(object sender, RoutedEventArgs routedEventArgs)
        {
            var scrollViewer = ControlFinder.FindParent<ScrollViewer>(this.AssociatedObject);

            if (scrollViewer == null)
            {
                return;
            }

            var hOffset = this.AssociatedObject.ActualHeight / 2;
            var vOffset = this.AssociatedObject.ActualWidth / 2;

            scrollViewer.ChangeView(hOffset, vOffset, 1, true);
        }
    }
}
