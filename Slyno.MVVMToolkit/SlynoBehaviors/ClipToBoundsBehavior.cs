﻿using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Microsoft.Xaml.Interactivity;

namespace Slyno.MVVMToolkit.SlynoBehaviors
{
    /// <summary>
    /// This will clip the children to the bounds of the attached element.
    /// Event listeners are setup to monitor size changes of the element.
    /// </summary>
    public sealed class ClipToBoundsBehavior : Behavior<FrameworkElement>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            this.AssociatedObject.SizeChanged += Element_SizeChanged;

            this.SetClip();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            this.AssociatedObject.SizeChanged -= Element_SizeChanged;
        }

        private void Element_SizeChanged(object sender, SizeChangedEventArgs sizeChangedEventArgs)
        {
            this.SetClip();
        }

        private void SetClip()
        {
            this.AssociatedObject.Clip = new RectangleGeometry
            {
                Rect = new Rect(0, 0, this.AssociatedObject.ActualWidth, this.AssociatedObject.ActualHeight)
            };
        }
    }
}
