﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Microsoft.Xaml.Interactivity;

namespace Slyno.MVVMToolkit.SlynoBehaviors
{
    /// <summary>
    /// This behavior will set focus to the given control as soon as it is loaded.
    /// </summary>
    public sealed class SetFocusBehavior : Behavior<Control>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            this.AssociatedObject.Loaded += ElementOnLoaded;
        }

        private void ElementOnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            this.AssociatedObject.Loaded -= ElementOnLoaded;

            this.AssociatedObject.Focus(FocusState.Programmatic);
        }
    }
}
