﻿using Windows.UI.Core;
using Windows.UI.Xaml;
using Microsoft.Xaml.Interactivity;

namespace Slyno.MVVMToolkit.SlynoBehaviors
{
    /// <summary>
    /// Makes the attached element's size match that of the window.
    /// Event listeners are setup to resize the element when the window resizes.
    /// This is useful for full window media.
    /// </summary>
    public sealed class FullWindowBehavior : Behavior<FrameworkElement>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            Window.Current.SizeChanged += WindowOnSizeChanged;

            this.SetSize();
        }

        private void WindowOnSizeChanged(object sender, WindowSizeChangedEventArgs e)
        {
            this.SetSize();
        }

        private void SetSize()
        {
            var bounds = Window.Current.Bounds;

            this.AssociatedObject.Width = bounds.Width;
            this.AssociatedObject.Height = bounds.Height;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            Window.Current.SizeChanged -= WindowOnSizeChanged;
        }
    }
}
