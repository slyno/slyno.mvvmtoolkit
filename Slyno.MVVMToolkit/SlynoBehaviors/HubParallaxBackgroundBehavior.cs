﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Microsoft.Xaml.Interactivity;

namespace Slyno.MVVMToolkit.SlynoBehaviors
{
    /// <summary>
    /// This behavior adds a parallax effect to the attached hub by scrolling it's image background at a different pace than the Hub's scroller.
    /// Default ParallaxRatio is .15. It has a min of 0 (no parallax) and a max of .9. A larger value amplifies the effect.
    /// </summary>
    public sealed class HubParallaxBackgroundBehavior : Behavior<Hub>
    {
        private ScrollViewer _scrollViewer;
        private TranslateTransform _translate;

        public static readonly DependencyProperty ParallaxRatioProperty = DependencyProperty.Register(
            "ParallaxRatio", typeof(double), typeof(HubParallaxBackgroundBehavior), new PropertyMetadata(.15));

        /// <summary>
        /// A value from 0 to 1 to determine the about of parallaxing.
        /// 0 means no parallax, 1 means the most possible.
        /// </summary>
        public double ParallaxRatio
        {
            get { return (double)GetValue(ParallaxRatioProperty); }
            set
            {
                if (value > .9)
                {
                    value = .9;
                }

                if (value < 0)
                {
                    value = 0;
                }

                SetValue(ParallaxRatioProperty, value);
            }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            this.AssociatedObject.Loaded += this.HubOnLoaded;
        }

        private void HubOnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            this.AssociatedObject.Loaded -= this.HubOnLoaded;

            var imageBrush = this.AssociatedObject.Background as ImageBrush;

            if (imageBrush == null)
            {
                return;
            }

            _scrollViewer = ControlFinder.FindParent<ScrollViewer>(this.AssociatedObject);

            if (_scrollViewer == null)
            {
                return;
            }

            _translate = new TranslateTransform
            {
                X = _scrollViewer.HorizontalOffset * this.ParallaxRatio
            };

            imageBrush.Transform = _translate;

            _scrollViewer.ViewChanged += ScrollViewerOnViewChanged;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            if (_scrollViewer == null)
            {
                return;
            }

            _scrollViewer.ViewChanged -= ScrollViewerOnViewChanged;
        }

        private void ScrollViewerOnViewChanged(object sender, ScrollViewerViewChangedEventArgs scrollViewerViewChangedEventArgs)
        {
            _translate.X = _scrollViewer.HorizontalOffset * this.ParallaxRatio;
        }
    }
}
