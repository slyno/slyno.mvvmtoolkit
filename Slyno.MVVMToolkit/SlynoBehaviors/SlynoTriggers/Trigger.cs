﻿using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Markup;
using Microsoft.Xaml.Interactivity;

namespace Slyno.MVVMToolkit.SlynoBehaviors.SlynoTriggers
{
    [ContentProperty(Name = "Actions")]
    public abstract class Trigger : Behavior
    {
        protected Trigger()
        {
            this.Actions = new ActionCollection();
        }

        public static readonly DependencyProperty ActionsProperty = DependencyProperty.Register(
            "Actions", typeof(ActionCollection), typeof(Trigger), new PropertyMetadata(default(ActionCollection)));

        public ActionCollection Actions
        {
            get { return (ActionCollection)GetValue(ActionsProperty); }
            set { SetValue(ActionsProperty, value); }
        }

        public void Execute(object sender, object parameter)
        {
            foreach (var item in this.Actions.OfType<IAction>())
            {
                item.Execute(sender, parameter);
            }
        }
    }

    [ContentProperty(Name = "Actions")]
    public abstract class Trigger<T> : Behavior<T> where T : DependencyObject
    {
        protected Trigger()
        {
            this.Actions = new ActionCollection();
        }

        public static readonly DependencyProperty ActionsProperty = DependencyProperty.Register(
            "Actions", typeof(ActionCollection), typeof(Trigger), new PropertyMetadata(default(ActionCollection)));

        public ActionCollection Actions
        {
            get { return (ActionCollection)GetValue(ActionsProperty); }
            set { SetValue(ActionsProperty, value); }
        }

        public void Execute(object sender, object parameter)
        {
            foreach (var item in this.Actions.OfType<IAction>())
            {
                item.Execute(sender, parameter);
            }
        }
    }
}
