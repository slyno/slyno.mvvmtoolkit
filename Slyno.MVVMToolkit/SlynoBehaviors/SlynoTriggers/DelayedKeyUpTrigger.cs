﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace Slyno.MVVMToolkit.SlynoBehaviors.SlynoTriggers
{
    /// <summary>
    /// This trigger will execute it's actions after the given delay and a KeyUp has occured.
    /// Useful for searching as everytime a KeyUp is detected, it will restart the timer. Default delay is 300 ms.
    /// </summary>
    public sealed class DelayedKeyUpTrigger : Trigger<FrameworkElement>
    {
        public static readonly DependencyProperty DelayProperty = DependencyProperty.Register(
            "Delay", typeof(TimeSpan), typeof(DelayedKeyUpTrigger), new PropertyMetadata(TimeSpan.FromMilliseconds(300)));

        /// <summary>
        /// Default is 300 ms.
        /// </summary>
        public TimeSpan Delay
        {
            get { return (TimeSpan)GetValue(DelayProperty); }
            set { SetValue(DelayProperty, value); }
        }

        private readonly DispatcherTimer _timer = new DispatcherTimer();
        private KeyRoutedEventArgs _keyRoutedEventArgs;

        public DelayedKeyUpTrigger()
        {
            _timer.Tick += (s, t) =>
            {
                _timer.Stop();
                this.Execute(this.AssociatedObject, _keyRoutedEventArgs);
            };
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            this.AssociatedObject.KeyUp += Element_OnKeyUp;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            _timer.Stop();
            this.AssociatedObject.KeyUp -= Element_OnKeyUp;
        }

        private void Element_OnKeyUp(object sender, KeyRoutedEventArgs e)
        {
            _keyRoutedEventArgs = e;

            _timer.Stop();
            _timer.Interval = this.Delay;
            _timer.Start();
        }
    }
}
