﻿using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace Slyno.MVVMToolkit.SlynoBehaviors.SlynoTriggers
{
    /// <summary>
    /// This trigger will execute it's actions whenever the given key has it's keyup event fired.
    /// </summary>
    public sealed class KeyUpTrigger : Trigger<FrameworkElement>
    {
        public static readonly DependencyProperty KeyProperty = DependencyProperty.Register(
            "Key", typeof(VirtualKey), typeof(KeyUpTrigger), new PropertyMetadata(default(VirtualKey)));

        public VirtualKey Key
        {
            get { return (VirtualKey)GetValue(KeyProperty); }
            set { SetValue(KeyProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            this.AssociatedObject.KeyUp += Element_KeyUp;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            this.AssociatedObject.KeyUp -= Element_KeyUp;
        }

        private void Element_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key != this.Key)
            {
                return;
            }

            this.Execute(sender, e);
        }
    }
}
