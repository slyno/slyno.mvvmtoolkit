﻿using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace Slyno.MVVMToolkit.SlynoBehaviors.SlynoTriggers
{
    /// <summary>
    /// This trigger will execute it's actions whenever the given key has it's keydown event fired.
    /// </summary>
    public sealed class KeyDownTrigger : Trigger<FrameworkElement>
    {
        public static readonly DependencyProperty KeyProperty = DependencyProperty.Register(
            "Key", typeof(VirtualKey), typeof(KeyDownTrigger), new PropertyMetadata(default(VirtualKey)));

        public VirtualKey Key
        {
            get { return (VirtualKey)GetValue(KeyProperty); }
            set { SetValue(KeyProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            this.AssociatedObject.KeyDown += Element_KeyDown;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            this.AssociatedObject.KeyDown -= Element_KeyDown;
        }

        private void Element_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key != this.Key)
            {
                return;
            }

            this.Execute(sender, e);
        }
    }
}
