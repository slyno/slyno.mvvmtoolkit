﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Slyno.MVVMToolkit.Extensions
{
    public static class CollectionEx
    {
        public static void AddRange<T>(this Collection<T> collection, IEnumerable<T> items)
        {
            var itemsList = items.ToList();

            foreach (var item in itemsList)
            {
                collection.Add(item);
            }
        }

        public static void RemoveRange<T>(this Collection<T> collection, IEnumerable<T> items)
        {
            var itemsList = items.ToList();

            foreach (var item in itemsList)
            {
                collection.Remove(item);
            }
        }

        public static void ReplaceAll<T>(this Collection<T> collection, IEnumerable<T> items)
        {
            collection.Clear();
            collection.AddRange(items);
        }
    }
}
