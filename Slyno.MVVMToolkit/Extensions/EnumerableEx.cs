﻿using System.Collections.Generic;

namespace Slyno.MVVMToolkit.Extensions
{
    public static class EnumerableEx
    {
        public static NotifyCollection<T> ToNotifyCollection<T>(this IEnumerable<T> enumerable)
        {
            return new NotifyCollection<T>(enumerable);
        }
    }
}
