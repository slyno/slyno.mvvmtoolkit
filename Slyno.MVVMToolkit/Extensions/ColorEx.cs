﻿using System;
using Windows.UI;

namespace Slyno.MVVMToolkit.Extensions
{
    public static class ColorEx
    {
        public static string ToHex(this Color color)
        {
            return $"#{color.A:X2}{color.R:X2}{color.G:X2}{color.B:X2}";
        }

        public static Color FromHex(string hex)
        {
            var defaultColor = Colors.Black;

            if (string.IsNullOrWhiteSpace(hex))
            {
                return defaultColor;
            }

            var cleanHex = hex.Replace("#", "");

            if (string.IsNullOrWhiteSpace(cleanHex))
            {
                return defaultColor;
            }

            try
            {
                switch (cleanHex.Length)
                {
                    case 2:
                        return Color.FromArgb(255,
                            Convert.ToByte(cleanHex.Substring(0, 1), 16),
                            Convert.ToByte(cleanHex.Substring(1, 1), 16),
                            Convert.ToByte(cleanHex.Substring(2, 1), 16));

                    case 6:
                        return Color.FromArgb(255,
                            Convert.ToByte(cleanHex.Substring(0, 2), 16),
                            Convert.ToByte(cleanHex.Substring(2, 2), 16),
                            Convert.ToByte(cleanHex.Substring(4, 2), 16));
                    case 8:
                        return Color.FromArgb(Convert.ToByte(cleanHex.Substring(0, 2), 16),
                            Convert.ToByte(cleanHex.Substring(2, 2), 16),
                            Convert.ToByte(cleanHex.Substring(4, 2), 16),
                            Convert.ToByte(cleanHex.Substring(6, 2), 16));
                    default:
                        return defaultColor;
                }
            }
            catch
            {
                return defaultColor;
            }
        }
    }
}
