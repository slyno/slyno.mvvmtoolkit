﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace Slyno.MVVMToolkit
{
    public static class ControlFinder
    {
        /// <summary>
        /// Finds this first child of the given type by traversing the visual tree.
        /// Returns null if it can't find any.
        /// </summary>
        /// <typeparam name="T">Type of FrameworkElement.</typeparam>
        /// <param name="element">Element to start at.</param>
        public static T FindChild<T>(FrameworkElement element) where T : FrameworkElement
        {
            if (element == null)
            {
                return null;
            }

            var childrenCount = VisualTreeHelper.GetChildrenCount(element);

            for (var i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(element, i);

                var childObj = child as T ?? FindChild<T>(child as FrameworkElement);

                if (childObj != null)
                {
                    return childObj;
                }
            }

            return null;
        }

        /// <summary>
        /// Finds the first parent of the given type by traversing the element's parent and the visual tree.
        /// Returns null it it can't find any.
        /// </summary>
        /// <typeparam name="T">Type of FrameworkElement.</typeparam>
        /// <param name="element">Element to start at.</param>
        public static T FindParent<T>(FrameworkElement element) where T : DependencyObject
        {
            if (element == null)
            {
                return null;
            }

            var parent = (element.Parent ?? VisualTreeHelper.GetParent(element)) as FrameworkElement;

            while (parent != null && !(parent is T))
            {
                parent = (parent.Parent ?? VisualTreeHelper.GetParent(parent)) as FrameworkElement;
            }

            return parent as T;
        }
    }
}
