Library includes some base helpers including:

NotifyObject - base class for Models and ViewModels to provide easy INotifyPropertyChanged implementations.

RelayCommand and AsyncRelayCommand - Implements ICommand to bind Commands to methods and async methods.

IDialogService and ILogger with some default implementations to help with MVVM applications.

[Nuget package available](https://www.nuget.org/packages/Slyno.MVVMToolkit/).
